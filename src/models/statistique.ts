export class Statistique {
	  id : String;
    title : String;
    value : String;
    appreciation : String;

	constructor(id : String, title : String, value : String, appreciation : String ) {
		this.id = id;
		this.title = title;
		this.value = value;
		this.appreciation = appreciation;
	}
}
