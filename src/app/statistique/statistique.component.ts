import { Component, OnInit, Input } from '@angular/core';
import { Statistique } from 'src/models/statistique';
import { StatistiqueService } from '../services/statistique/statistique.service';

@Component({
  selector: 'app-statistique',
  templateUrl: './statistique.component.html',
  styleUrls: ['./statistique.component.css']
})
export class StatistiqueComponent implements OnInit {
  stats: Statistique[] = []; // Statistics tab from stats.naminilamy.fr api
  mouse_id: String = ""; // use for identify div hovered

  constructor(private statistiqueService: StatistiqueService) {}

  getStatistiques(): void {
    this.statistiqueService.getStatistiques()
      .subscribe(stats => this.stats = stats);
  }

  ngOnInit(): void {
    this.getStatistiques();
  }

  delete(stat: Statistique): void {
    this.statistiqueService.deleteStatistique(stat.id).subscribe(() => this.stats = this.stats.filter(s => s !== stat));
  }

  // Call by mouseover in the template
  changeStyle(stat: Statistique): void {
    this.mouse_id = stat.id;
  }

  // Call by ngClass in the template for select the correct appreciation of stat
  getClass(stat: Statistique){
    return stat.id === this.mouse_id ? stat.appreciation : '';
  }
}
