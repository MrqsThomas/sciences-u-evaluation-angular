import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Statistique } from 'src/models/statistique';

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {

  private url = 'https://stats.naminilamy.fr/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  /** GET: get the statistic from the server */
  getStatistiques(): Observable<Statistique[]> {
    return this.http.get<Statistique[]>(this.url)
  }

  /** DELETE: delete the statistic from the server */
  deleteStatistique(id: String): Observable<Statistique> {
    const url = `${this.url}${id}`;

    return this.http.delete<Statistique>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted statistic id=${id}`)),
      catchError(this.handleError<Statistique>('deleteHero'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
   private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
